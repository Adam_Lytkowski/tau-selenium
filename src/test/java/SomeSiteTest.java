//package com.example.seleniumdemo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class SomeSiteTest {
    private WebDriver driver;
    private String baseUrl;
//    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
//        driver = new ChromeDriver();
        driver = new FirefoxDriver();
//        baseUrl = "http://puzniakowski.pl/";
        baseUrl = "http://automationpractice.com";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void isInvalidEmailTextDisplayed() throws Exception {
        driver.get(baseUrl);
        driver.findElement(By.className("login")).click();
        WebElement createAccountButton = driver.findElement(By.id("SubmitCreate"));
        assertEquals(true, createAccountButton.isDisplayed());
        createAccountButton.click();
        WebElement allertField = driver.findElement(By.id("create_account_error"));
        WebElement invalidEmailText = allertField.findElement(By.xpath("//*[contains(text(), 'Invalid email address.')]"));
        assertEquals(true, allertField.isDisplayed());
        assertEquals(true, invalidEmailText.isDisplayed());

    }

    @Test
    public void formValidationTest() throws Exception {
        driver.get(baseUrl);
        driver.findElement(By.className("login")).click();
        
        WebElement emailAddressField = driver.findElement(By.id("email_create"));
        assertEquals(true, emailAddressField.isDisplayed());
        emailAddressField.sendKeys("test12345@wp.pl");
        
        WebElement createAccountButton = driver.findElement(By.id("SubmitCreate"));
        assertEquals(true, createAccountButton.isDisplayed());
        createAccountButton.click();
        
        
        WebElement company = driver.findElement(By.id("company"));
        assertEquals(true, company.isDisplayed());
        
        WebElement firstName = driver.findElement(By.id("customer_firstname"));
        assertEquals(true, firstName.isDisplayed());
        firstName.sendKeys("111");
        company.click();
        WebElement formError = driver.findElement(By.className("form-error"));
        assertEquals(true, formError.isDisplayed());
        firstName.sendKeys("Adam");
        
        WebElement lastName = driver.findElement(By.id("customer_lastname"));
        assertEquals(true, lastName.isDisplayed());
        lastName.sendKeys("111");
        company.click();
        assertEquals(true, formError.isDisplayed());
        lastName.sendKeys("Nowak");
        
        WebElement password = driver.findElement(By.id("passwd"));
        assertEquals(true, password.isDisplayed());
        password.sendKeys("111");
        company.click();
        assertEquals(true, formError.isDisplayed());
        password.sendKeys("12345");
        
        
        WebElement firstNameAddress = driver.findElement(By.id("firstname"));
        assertEquals(true, firstNameAddress.isDisplayed());
        WebElement lastNameAddress = driver.findElement(By.id("lastname"));
        assertEquals(true, lastNameAddress.isDisplayed());
        WebElement address = driver.findElement(By.id("address1"));
        assertEquals(true, address.isDisplayed());
        WebElement city = driver.findElement(By.id("city"));
        assertEquals(true, city.isDisplayed());
        WebElement state = driver.findElement(By.id("id_state"));
        assertEquals(true, state.isDisplayed());
        WebElement postalCode = driver.findElement(By.id("postcode"));
        assertEquals(true, postalCode.isDisplayed());
        WebElement country = driver.findElement(By.id("id_country"));
        assertEquals(true, country.isDisplayed());
        WebElement mobilePhone = driver.findElement(By.id("phone_mobile"));
        assertEquals(true, mobilePhone.isDisplayed());

    }
    
    @Test
    public void successfulRegistrationTest() throws Exception {
        driver.get(baseUrl);
        driver.findElement(By.className("login")).click();
        
        WebElement emailAddressField = driver.findElement(By.id("email_create"));
       // assertEquals(true, emailAddressField.isDisplayed());
        emailAddressField.sendKeys("test12345@wp.pl");
        
        WebElement createAccountButton = driver.findElement(By.id("SubmitCreate"));
       // assertEquals(true, createAccountButton.isDisplayed());
        createAccountButton.click();
        
      
        
        WebElement firstName = driver.findElement(By.id("customer_firstname"));
       // assertEquals(true, firstName.isDisplayed());
        firstName.sendKeys("Adam");
        
        WebElement lastName = driver.findElement(By.id("customer_lastname"));
        lastName.sendKeys("Nowak");
        
        WebElement password = driver.findElement(By.id("passwd"));
      //  assertEquals(true, password.isDisplayed());  
        password.sendKeys("12345");
        
        WebElement firstNameAddress = driver.findElement(By.id("firstname"));
       // assertEquals(true, firstNameAddress.isDisplayed());
        firstNameAddress.sendKeys("Adam");
        
        WebElement lastNameAddress = driver.findElement(By.id("lastname"));
      //  assertEquals(true, lastNameAddress.isDisplayed());
        lastNameAddress.sendKeys("Nowak");
        
        WebElement address = driver.findElement(By.id("address1"));
       // assertEquals(true, address.isDisplayed());
        address.sendKeys("Nowakowa");
        
        WebElement city = driver.findElement(By.id("city"));
       // assertEquals(true, city.isDisplayed());
        city.sendKeys("Gdansk");
        
        WebElement state = driver.findElement(By.id("id_state"));
      //  assertEquals(true, state.isDisplayed());
        Select stateDropdown = new Select(state);
        stateDropdown.selectByVisibleText("California");
        
        WebElement postalCode = driver.findElement(By.id("postcode"));
       // assertEquals(true, postalCode.isDisplayed());
        postalCode.sendKeys("90002");
        
        WebElement country = driver.findElement(By.id("id_country"));
      //  assertEquals(true, country.isDisplayed());
        Select countryDropdown = new Select(country);
        stateDropdown.selectByValue("21");
        
        WebElement mobilePhone = driver.findElement(By.id("phone_mobile"));
      //  assertEquals(true, mobilePhone.isDisplayed());
        mobilePhone.sendKeys("666777888");
        
        
        WebElement submitAccount = driver.findElement(By.id("submitAccount"));
      //  assertEquals(true, submitAccount.isDisplayed());
        submitAccount.click();
        
        WebElement infoAccount = driver.findElement(By.className("info-account"));
        assertEquals(true, infoAccount.isDisplayed());

    }
    
    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

//    private String closeAlertAndGetItsText() {
//        try {
//            Alert alert = driver.switchTo().alert();
//            String alertText = alert.getText();
//            if (acceptNextAlert) {
//                alert.accept();
//            } else {
//                alert.dismiss();
//            }
//            return alertText;
//        } finally {
//            acceptNextAlert = true;
//        }
//    }
}